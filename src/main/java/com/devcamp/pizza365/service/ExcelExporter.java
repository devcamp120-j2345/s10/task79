package com.devcamp.pizza365.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.pizza365.entity.Customer;

public class ExcelExporter {
    private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<Customer> customers;

	public ExcelExporter(List<Customer> customers) {
		this.customers = customers;
		workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("customers");
	}

    public void writeCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        cell.setCellStyle(style);

        if (value  instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
        
    }

    public void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();        
        style.setFont(font);

        for(Customer customer: customers) {
            Row row = sheet.createRow(rowCount);
            rowCount++;
            writeCell(row, 0, customer.getId(), style);
            writeCell(row, 1, customer.getFirstName(), style);
            writeCell(row, 2, customer.getLastName(), style);
            writeCell(row,3, customer.getPhoneNumber(), style);
            writeCell(row,4, customer.getAddress(), style);
            writeCell(row,5, customer.getCity(), style);
            writeCell(row,6, customer.getCountry(), style);
        }
    }

    public void writeHeaders() {
        Row row = sheet.createRow(0);
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);

        // Write cells.
        writeCell(row, 0, "UserId", style);
        writeCell(row, 1, "First Name", style);
        writeCell(row, 2, "Last Name", style);
        writeCell(row,3, "Phone Number", style);
        writeCell(row,4, "Address", style);
        writeCell(row,5, "City", style);
        writeCell(row,6, "Country", style);
    }

    public void export(HttpServletResponse response) throws IOException {
        // Write header titles into sheet.
        writeHeaders();
        // Write rows data into sheet.
        writeDataLines();
        // Put Excel data into stream output
        ServletOutputStream stream = response.getOutputStream();
        workbook.write(stream);
        // Close Stream output/
        stream.close();
    }
    
}
